Ctools tabs levels
http://drupal.org/sandbox/Hydra/1970916
====================================


DESCRIPTION
-----------
Ctools tabs levels extends ctools content_type for Tabs to show deeper levels 
then secondary. This might be usefull, if you want to render a separated block 
with third-level tabs or deeper. Without this module only primary and secondary 
tabs are available.


REQUIREMENTS
------------
Drupal 7.x
ctools - http://drupal.org/project/ctools


INSTALLING
----------
1. To install the module copy the 'ctools_tabs_levels' folder to your 
	 sites/all/modules directory.
2. Read more about installing modules at http://drupal.org/node/70151


CONFIGURING AND USING
---------------------
1. Go to a panel to add a pane to a region. Use the default Tabs content_type
	 find under "Page Elements" -> "Tabs".
1. Select "Other" and configure the depth of the level.


REPORTING ISSUE. REQUESTING SUPPORT. REQUESTING NEW FEATURE
-----------------------------------------------------------
1. Go to the module issue queue at http://drupal.org/project/issues/1970916?categories=All
2. Click on CREATE A NEW ISSUE link.
3. Fill the form.
4. To get a status report on your request go to http://drupal.org/project/issues/user
