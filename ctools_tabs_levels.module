<?php

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Hook the ctools_page_tabs_content_type_edit_form to add options to
 * select the level of a rendered tab.
 */
function ctools_tabs_levels_form_ctools_page_tabs_content_type_edit_form_alter(&$form, &$form_state, $form_id) {
  $conf = $form_state['conf'];

  $form['type']['#options'] += array('other' => t('Other Level'));

  $form['level'] = array(
    '#type' => 'select',
    '#title' => t('Tab level'),
    '#states' => array(
      'visible' => array(
        ':input[name="type"]' => array('value' => 'other'),
      ),
    ),
    '#options' => array(
      2 => t('Third tabs'),
      3 => t('Fourth tabs'),
      4 => t('Fifth tabs'),
      5 => t('Sixth tabs'),
      6 => t('Seventh tabs'),
      7 => t('Eighth tabs'),
      8 => t('Ninth tabs'),
    ),
    '#default_value' => isset($conf['level']) ? $conf['level'] : 2,
    '#weight' => 1
  );

  $form['id']['#weight'] = 2;
}

/**
 * Implements hook_ctools_plugin_pre_alter().
 *
 * Change the render callback to use our own.
 * Add the level to default, otherwise ctools won't save the value.
 */
function ctools_tabs_levels_ctools_plugin_pre_alter(&$plugin, &$info) {
  if($plugin['name'] == 'page_tabs') {
    $plugin['render callback'] = 'ctools_tabs_levels_page_tabs_content_type_render';
    $plugin['defaults'] += array('level' => 2);
  }
}

/**
 * Implementation of ctools_tabs_levels_page_tabs_content_type_render().
 *
 * Replace for ctools_page_tabs_content_type_render. If other is selected, we will
 * render the right level of tabs.
 */
function ctools_tabs_levels_page_tabs_content_type_render($subtype, $conf, $panel_args) {
  if($conf['type'] == 'other') {
    $block = new stdClass();
    
    $links = menu_local_tasks($conf['level']);
    $menus = $links['tabs']['count'] > 1 ? $links['tabs']['output'] : '';

    if ($conf['id']) {
      $menus['#theme_wrappers'][] = 'ctools_tabs_levels_wrapper';
      $menus['#attributes']['id'] = $conf['id'];
    }

    $block->content = $menus;
    return $block;
  }

  return ctools_page_tabs_content_type_render($subtype, $conf, $panel_args);
}

/**
 * Implementation of hook_theme().
 */
function ctools_tabs_levels_theme() {
  return array(
    'ctools_tabs_levels_wrapper' => array(
      'render element' => 'links',
      'file' => 'ctools-tabs-levels.theme.inc',
    ),
  );
}
