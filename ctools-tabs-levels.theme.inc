<?php

/**
 * Implementation of theme_ctools_tabs_levels_wrapper().
 *
 * Render a tab wrapper. This is basiclally theme_container.
 */
function theme_ctools_tabs_levels_wrapper($variables) {
	$element = $variables['links'];
  return '<ul' . drupal_attributes($element['#attributes']) . '>' . $element['#children'] . '</ul>';
}
